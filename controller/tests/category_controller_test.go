package tests

import (
	"bytes"
	"encoding/json"
	"github.com/golang/mock/gomock"
	"github.com/julienschmidt/httprouter"
	"momodding/rest-api/controller"
	"momodding/rest-api/model/web"
	"momodding/rest-api/service/mock"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestFindByIdController(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCategoryService := mock.NewMockCategoryService(mockCtrl)
	webResponse := web.CategoryResponse{
		Id:   1,
		Name: "Sukses",
	}

	mockCategoryService.EXPECT().FindById(gomock.Any(), gomock.Any()).Return(webResponse).Times(1)

	testImpl := &controller.CategoryControllerImpl{
		CategoryService: mockCategoryService,
	}

	var body bytes.Buffer
	_ = json.NewEncoder(&body).Encode("")

	req, _ := http.NewRequest("GET", "", &body)
	res := httptest.NewRecorder()
	param := httprouter.Param{Key: "id", Value: "1"}
	params := httprouter.Params{param}

	testImpl.FindById(res, req, params)
}

func TestFindAllController(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCategoryService := mock.NewMockCategoryService(mockCtrl)
	webResponse := []web.CategoryResponse{
		{
			Id:   1,
			Name: "Sukses",
		},
	}

	mockCategoryService.EXPECT().FindAll(gomock.Any()).Return(webResponse).Times(1)

	testImpl := &controller.CategoryControllerImpl{
		CategoryService: mockCategoryService,
	}

	var body bytes.Buffer
	_ = json.NewEncoder(&body).Encode("")

	req, _ := http.NewRequest("GET", "", &body)
	res := httptest.NewRecorder()
	param := httprouter.Param{Key: "id", Value: "1"}
	params := httprouter.Params{param}

	testImpl.FindAll(res, req, params)
}

func TestCreateController(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCategoryService := mock.NewMockCategoryService(mockCtrl)
	webResponse := web.CategoryResponse{
		Id:   1,
		Name: "Sukses",
	}

	mockCategoryService.EXPECT().Create(gomock.Any(), gomock.Any()).Return(webResponse).Times(1)

	testImpl := &controller.CategoryControllerImpl{
		CategoryService: mockCategoryService,
	}

	webRequest := web.CategoryCreateRequest{
		Name: "Sukses",
	}

	var body bytes.Buffer
	_ = json.NewEncoder(&body).Encode(webRequest)

	req, _ := http.NewRequest("POST", "", &body)
	res := httptest.NewRecorder()
	param := httprouter.Param{Key: "id", Value: "1"}
	params := httprouter.Params{param}

	testImpl.Create(res, req, params)
}

func TestUpdateController(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCategoryService := mock.NewMockCategoryService(mockCtrl)
	webResponse := web.CategoryResponse{
		Id:   1,
		Name: "Sukses",
	}

	mockCategoryService.EXPECT().Update(gomock.Any(), gomock.Any()).Return(webResponse).Times(1)

	testImpl := &controller.CategoryControllerImpl{
		CategoryService: mockCategoryService,
	}

	webRequest := web.CategoryUpdateRequest{
		Id:   1,
		Name: "Sukses",
	}

	var body bytes.Buffer
	_ = json.NewEncoder(&body).Encode(webRequest)

	req, _ := http.NewRequest("POST", "", &body)
	res := httptest.NewRecorder()
	param := httprouter.Param{Key: "id", Value: "1"}
	params := httprouter.Params{param}

	testImpl.Update(res, req, params)
}

func TestDeleteController(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCategoryService := mock.NewMockCategoryService(mockCtrl)
	mockCategoryService.EXPECT().Delete(gomock.Any(), gomock.Any()).Times(1)

	testImpl := &controller.CategoryControllerImpl{
		CategoryService: mockCategoryService,
	}

	var body bytes.Buffer
	_ = json.NewEncoder(&body).Encode("")

	req, _ := http.NewRequest("POST", "", &body)
	res := httptest.NewRecorder()
	param := httprouter.Param{Key: "id", Value: "1"}
	params := httprouter.Params{param}

	testImpl.Delete(res, req, params)
}
