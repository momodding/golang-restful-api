package controller

import (
	"github.com/julienschmidt/httprouter"
	"momodding/rest-api/helper"
	"momodding/rest-api/model/web"
	"momodding/rest-api/service"
	"net/http"
	"strconv"
)

type CategoryControllerImpl struct {
	CategoryService service.CategoryService
}

func NewCategoryController(categoryService service.CategoryService) *CategoryControllerImpl {
	return &CategoryControllerImpl{
		CategoryService: categoryService,
	}
}

func (controller *CategoryControllerImpl) Create(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	request := web.CategoryCreateRequest{}
	helper.ReadFromRequestBody(r, &request)

	result := controller.CategoryService.Create(r.Context(), request)
	response := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   result,
	}

	helper.WriteToResponseBody(w, response)
}

func (controller *CategoryControllerImpl) Update(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	request := web.CategoryUpdateRequest{}
	helper.ReadFromRequestBody(r, &request)

	result := controller.CategoryService.Update(r.Context(), request)
	response := web.WebResponse{
		Code:   201,
		Status: "Created",
		Data:   result,
	}

	helper.WriteToResponseBody(w, response)
}

func (controller *CategoryControllerImpl) Delete(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	paramId := params.ByName("id")
	id, err := strconv.Atoi(paramId)
	helper.PanicIfError(err)

	controller.CategoryService.Delete(r.Context(), id)
	response := web.WebResponse{
		Code:   204,
		Status: "No Content",
	}

	helper.WriteToResponseBody(w, response)
}

func (controller *CategoryControllerImpl) FindById(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	paramId := params.ByName("id")
	id, err := strconv.Atoi(paramId)
	helper.PanicIfError(err)

	result := controller.CategoryService.FindById(r.Context(), id)
	response := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   result,
	}

	helper.WriteToResponseBody(w, response)
}

func (controller *CategoryControllerImpl) FindAll(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	result := controller.CategoryService.FindAll(r.Context())
	response := web.WebResponse{
		Code:   200,
		Status: "OK",
		Data:   result,
	}

	helper.WriteToResponseBody(w, response)
}
