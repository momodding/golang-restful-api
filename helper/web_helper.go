package helper

import (
	"encoding/json"
	"net/http"
)

func ReadFromRequestBody(r *http.Request, obj interface{}) {
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(obj)
	PanicIfError(err)
}

func WriteToResponseBody(w http.ResponseWriter, obj interface{}) {
	w.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err := encoder.Encode(obj)
	PanicIfError(err)
}
