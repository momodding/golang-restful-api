package service

import (
	"context"
	"database/sql"
	"github.com/go-playground/validator/v10"
	"momodding/rest-api/exception"
	"momodding/rest-api/helper"
	"momodding/rest-api/model/domain"
	"momodding/rest-api/model/web"
	"momodding/rest-api/repository"
)

type CategoryServiceImpl struct {
	CategoryRepo repository.CategoryRepository
	DB           *sql.DB
	Validate     *validator.Validate
}

func NewCategoryService(categoryRepository repository.CategoryRepository, db *sql.DB, validate *validator.Validate) *CategoryServiceImpl {
	return &CategoryServiceImpl{
		CategoryRepo: categoryRepository,
		DB:           db,
		Validate:     validate,
	}
}

func (service *CategoryServiceImpl) Create(ctx context.Context, request web.CategoryCreateRequest) web.CategoryResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	category := domain.Category{
		Name: request.Name,
	}

	category = service.CategoryRepo.Save(ctx, tx, category)

	return web.ToCategoryResponse(category)
}

func (service *CategoryServiceImpl) Update(ctx context.Context, request web.CategoryUpdateRequest) web.CategoryResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	category, err := service.CategoryRepo.FindById(ctx, tx, request.Id)
	exception.ThrowNotFoundError(err)

	category = domain.Category{
		Id:   request.Id,
		Name: request.Name,
	}

	category = service.CategoryRepo.Update(ctx, tx, category)

	return web.ToCategoryResponse(category)
}

func (service *CategoryServiceImpl) Delete(ctx context.Context, id int) {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	_, err = service.CategoryRepo.FindById(ctx, tx, id)
	exception.ThrowNotFoundError(err)

	service.CategoryRepo.Delete(ctx, tx, id)
}

func (service *CategoryServiceImpl) FindById(ctx context.Context, id int) web.CategoryResponse {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	category, err := service.CategoryRepo.FindById(ctx, tx, id)
	exception.ThrowNotFoundError(err)

	return web.ToCategoryResponse(category)
}

func (service *CategoryServiceImpl) FindAll(ctx context.Context) []web.CategoryResponse {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	categories := service.CategoryRepo.FindAll(ctx, tx)
	var categoriesResponse []web.CategoryResponse
	for _, category := range categories {
		categoryResponse := web.ToCategoryResponse(category)
		categoriesResponse = append(categoriesResponse, categoryResponse)
	}

	return categoriesResponse
}
