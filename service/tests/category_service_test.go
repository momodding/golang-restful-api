package tests

import (
	"github.com/golang/mock/gomock"
	"momodding/rest-api/model/domain"
	"momodding/rest-api/repository/mock"
	"momodding/rest-api/service"
	"net/http"
	"testing"
)

func TestFindByIdService(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCategoryRepository := mock.NewMockCategoryRepository(mockCtrl)
	category := domain.Category{
		Id:   1,
		Name: "category",
	}

	mockCategoryRepository.EXPECT().FindById(gomock.Any(), gomock.Any(), gomock.Any()).Return(category)

	testImpl := &service.CategoryServiceImpl{
		CategoryRepo: mockCategoryRepository,
	}

	req, _ := http.NewRequest("GET", "", nil)

	testImpl.FindById(req.Context(), 1)
}
