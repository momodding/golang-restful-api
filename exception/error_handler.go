package exception

import (
	"github.com/go-playground/validator/v10"
	"momodding/rest-api/helper"
	"momodding/rest-api/model/web"
	"net/http"
)

func ErrorHandler(w http.ResponseWriter, r *http.Request, err interface{}) {
	if notFoundError(w, r, err) {
		return
	}

	if validationErrors(w, r, err) {
		return
	}

	if unauthorizedErrors(w, r, err) {
		return
	}

	internalServerError(w, r, err)
}

func unauthorizedErrors(w http.ResponseWriter, r *http.Request, err interface{}) bool {
	exception, isError := err.(UnauthorizedError)
	if isError {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		response := web.WebResponse{
			Code:   http.StatusUnauthorized,
			Status: "Unauthorized",
			Data:   exception.Error,
		}
		helper.WriteToResponseBody(w, response)
		return true
	}

	return false
}

func validationErrors(w http.ResponseWriter, r *http.Request, err interface{}) bool {
	exception, isError := err.(validator.ValidationErrors)
	if isError {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		response := web.WebResponse{
			Code:   http.StatusBadRequest,
			Status: "Bad Request",
			Data:   exception.Error(),
		}
		helper.WriteToResponseBody(w, response)
		return true
	}

	return false
}

func notFoundError(w http.ResponseWriter, r *http.Request, err interface{}) bool {
	exception, isError := err.(NotFoundError)
	if isError {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		response := web.WebResponse{
			Code:   http.StatusNotFound,
			Status: "Not Found",
			Data:   exception.Error,
		}
		helper.WriteToResponseBody(w, response)
		return true
	}

	return false
}

func internalServerError(w http.ResponseWriter, r *http.Request, err interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	response := web.WebResponse{
		Code:   http.StatusInternalServerError,
		Status: "Server Error",
		Data:   err,
	}
	helper.WriteToResponseBody(w, response)
}
