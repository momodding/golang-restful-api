package exception

type NotFoundError struct {
	Error string
}

func NewNotFoundError(err string) NotFoundError {
	return NotFoundError{Error: err}
}

func ThrowNotFoundError(err error) {
	panic(NewNotFoundError(err.Error()))
}
