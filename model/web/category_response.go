package web

import "momodding/rest-api/model/domain"

type CategoryResponse struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

func ToCategoryResponse(category domain.Category) CategoryResponse {
	return CategoryResponse{
		Id:   category.Id,
		Name: category.Name,
	}
}
