package config

import (
	"database/sql"
	"momodding/rest-api/helper"
	"time"
)

func NewDB() *sql.DB {
	db, err := sql.Open("mysql", "root:@tcp(localhost:3306)/golang-rest-api")
	helper.PanicIfError(err)
	//defer func(db *sql.DB) {
	//	err := db.Close()
	//	helper.PanicIfError(err)
	//}(db)

	db.SetMaxIdleConns(5)
	db.SetMaxOpenConns(20)
	db.SetConnMaxLifetime(60 * time.Minute)
	db.SetConnMaxIdleTime(10 * time.Minute)
	return db
}
