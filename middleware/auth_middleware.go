package middleware

import (
	"momodding/rest-api/helper"
	"momodding/rest-api/model/web"
	"net/http"
)

type AuthMiddleware struct {
	Handler http.Handler
}

func NewAuthMiddleware(handler http.Handler) *AuthMiddleware {
	return &AuthMiddleware{Handler: handler}
}

func (mware *AuthMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if "RAHASIA" != r.Header.Get("X-Api-Key") {
		//exception.ThrowUnauthorizedError(errors.New("unauthorized"))
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		response := web.WebResponse{
			Code:   http.StatusUnauthorized,
			Status: "Unauthorized",
		}
		helper.WriteToResponseBody(w, response)
	} else {
		mware.Handler.ServeHTTP(w, r)
	}
}
